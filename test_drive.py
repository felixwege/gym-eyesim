import argparse
import gym
import random
import time


def test_drive(path_to_sim):
    # Create eyebot env
    env = gym.make("gym_eyesim:eyebot-v0", path_to_sim=path_to_sim)
    env.reset()

    try:
        while True:
            # Start timer
            start_time = time.time()

            # Random action
            action = random.uniform(-1, 1)
            _, _, done, _ = env.step(action)

            # Reset if done
            if done:
                env.reset()

            # Synchronization
            diff = time.time() - start_time
            wait = 1 / 10 - diff
            if wait > 0:
                time.sleep(wait)
    except KeyboardInterrupt:
        env.close()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("path_to_sim", type=str)
    args = parser.parse_args()
    return args.path_to_sim


if __name__ == "__main__":
    path_to_sim = parse_args()
    test_drive(path_to_sim)
