# gym-eyesim

### Installation

Follow the [instructions](http://robotics.ee.uwa.edu.au/eyesim/) to install EyeSim.
Make sure `eyesim` and `eye.py` are in your path.

`
pip install gym-eyesim
`


### Usage

`
env = gym.make("gym_eyesim:eyebot-v0", path_to_sim=path_to_sim)
`

See [\_\_init\_\_.py](https://gitlab.com/felixwege/gym-eyesim/blob/master/gym_eyesim/__init__.py) for available environments.
